$(document).ready(function () {
  $("form").submit(function (event) {
      event.preventDefault();
      digestMessage($("#password").val())
      .then(digestBuffer => {
          var formData = {
              npm: $("#npm").val(),
              password: digestBuffer
          };
          $.ajax({
              type: "POST",
              url: "https://api.korpstar-poltekssn.org:8443/api/auth/signin",
              data: formData,
              dataType: "json",
              encode: true,
          }).done(function (data) {
              Cookies.set('accessToken', data.accessToken, { expires: 1 });
              window.location.href = "/dashboard"
          }).fail(function (xhr) {
              alert('Login failed');
          });
      });
  });
});
