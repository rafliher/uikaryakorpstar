$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "https://api.korpstar-poltekssn.org:8443/api/user/profile",
        headers: { 'Authorization': `Bearer ${Cookies.get('accessToken')}` }
    }).done(function (data) {
        $('#fullname').html(data.fullname);
        $('#npm').html(data.npm);
        $('#class').html(data.class);
        if (data.room) {
            $('#room').html(data.room.string);
        } else {
            $('#room').html('');
        }
    }).fail(function (xhr) {
        alert('Authorization failed');
        window.location.href = "/login"
    });

    $("#btn-ttefile").click(function (event) {
        fetch('https://api.korpstar-poltekssn.org:8443/api/user/digitalsignaturefile',{
            method: "GET",
            headers: { 'Authorization': `Bearer ${Cookies.get('accessToken')}` }
        }).then(response => response.blob())
          .then(blob => window.open(URL.createObjectURL(blob), "_blank"));
    });

    $("#btn-ttepw").click(function (event) {
        console.log('s');
        $.ajax({
            type: "GET",
            url: "https://api.korpstar-poltekssn.org:8443/api/user/digitalsignaturepassword",
            headers: { 'Authorization': `Bearer ${Cookies.get('accessToken')}` }
        }).done(function (data) {
            console.log(data);
            alert(data.password);
        }).fail(function (xhr) {
            alert('Authorization failed');
            window.location.href = "/login"
        });
    });

});
